package org.villablanca;
/**
 * Clase que representa un curso con sus asignaturas y nombre de ciclo.
 * @author laura
 *
 */
public class Curso {

	private String nombreCiclo;
	private int curso;
	private Asignatura[] asignatura;
	private int cantidadAsignaturas;
	
	/**
	 * Crea un curso y establece el número totalde asignaturas que tendrá
	 * @param nombreCiclo Nombre que tendrá el ciclo
	 * @param curso Numero del curso (P.Ej: 1º Curso, 2º...etc)
	 * @param asignaturasTotales Numero total (longitud del Array) de asignaturas que tendra el curso 
	 * @param cantidadAsignaturas Cantidad de asignaturas dadas de alta actualmente en el Curso (inicializada a 0)
	 */
	public Curso(String nombreCiclo, int curso, int asignaturasTotales) {
		this.nombreCiclo = nombreCiclo;
		this.curso = curso;
		asignatura = new Asignatura[asignaturasTotales];
		cantidadAsignaturas = 0;
	}
	
	/**
	 * Método que agrega una nueva asignatura al array de Asignaturas y aumenta en +1 la cantidad actual de las mismas
	 * @param asignaturaNueva Recibe por parámetro la asignatura a añadir.
	 */
	public void addAsignatura(Asignatura asignaturaNueva) {
		asignatura[cantidadAsignaturas] = asignaturaNueva;
		cantidadAsignaturas++;
	}
	
	
}
