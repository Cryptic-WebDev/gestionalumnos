package org.villablanca;

/**
 * Clase que representa un alumno con sus datos
 * @author Christian
 *
 */

public class Alumno {
	private int nia;
	private String nombre;
	private String apellidos;
	private String dni;
	private Fecha fechaNacimiento;
	private Asignatura[] asignaturas;
	private int cantidadAsignaturas;

	/**
	 * Crea un Alumno con sus datos personales
	 * @param nia Numero de Matricula
	 * @param nombre Nombre del Alumno
	 * @param apellidos Apellidos del Alumno
	 * @param dni DNI del Alumno
	 * @param fecha fecha nacimiento del Alumno
	 * @param asignaturasTotales Numero total (longitud del Array) de asignaturas que tendra el alumno
	 */
	public Alumno(int nia, String nombre, String apellidos, String dni, Fecha fechaNacimiento, int asignaturasTotales) {
		this.nia = nia;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.dni = dni;
		this.fechaNacimiento = fechaNacimiento;
		asignaturas = new Asignatura[asignaturasTotales];
		cantidadAsignaturas = 0;
		
	}
	
	/**
	 * Método que agrega una nueva asignatura al array de Asignaturas y aumenta en +1 la cantidad actual de las mismas
	 * @param asignaturaNueva Recibe por parámetro la asignatura a añadir.
	 */
	public void addAsignatura(Asignatura asignaturaNueva) {
		asignaturas[cantidadAsignaturas] = asignaturaNueva;
		cantidadAsignaturas++;
	}
}
