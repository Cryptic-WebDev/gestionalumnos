package org.villablanca;


/**
 * Clase que representa las notas de una asignatura de un alumno
 * @author carlos
 *
 */


public class Nota {

	private Alumno alumno;
	private Asignatura asignatura;
	private double nota[];
	
	
	/**
	 * Crea una nota que contiene un array de notas de cada evaluacion
	 * @param alumno alumno el cual pertenece la nota
	 * @param asignatura asignatura la cual pertenece la nota
	 */
	
	public Nota(Alumno alumno, Asignatura asignatura) {
		
		this.alumno = alumno;
		this.asignatura = asignatura;
		nota = new double [5];
	}
	
/**
 * Metodo que añade nota
 * @param evaluacion evaluacion de la nota
 * @param nota nota de la evaluacion
 */
	
	public void addNota(int evaluacion, double nota) {
		this.nota[evaluacion] = nota;
	}
	
	/**
	 * Devuelve la nota de la evaluacion especificada
	 * @param evaluacion nota de la evaluacion
	 * @return devuelve la nota
	 */
	
	public double getNota(int evaluacion) {
		return this.nota[evaluacion];
	}
	
}
