package org.villablanca;

/**
 * Clase que representa las asignaturas del curso
 * @author ivan
 *
 */

public class Asignatura {

	private String nombre;
	private Curso curso;
	private int codigo;
	
	/**
	 * Crea un Asignatura con los datos de cada asignatura
	 * @param nombre Nombre de la asignatura
	 * @param curso Curso de la asignatura
	 * @param codigo Codigo de la asignatura
	 */
	
	public Asignatura(String nombre, Curso curso, int codigo) {
		this.nombre = nombre;
		this.curso = curso;
		this.codigo= codigo;
	}


	
	
	
}
