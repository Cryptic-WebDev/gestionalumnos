package org.villablanca;

import daw.com.Teclado;

/**
 * Clase Main (Principal) que ejecutará el programa de GestionAlumnos
 * @author Christian
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	/**
	 * Función que pide los datos de un alumno y lo añade al array de alumnos un alumno
	 * @param alumnos Array de Alumnos a modificar
	 * @param posicionLibre Ultimas posicion libre del array de alumnos
	 */
	public void altaAlumno(Alumno[] alumnos, int posicionLibre) {
		int day, month, year, asignaturasTotales, nia;
		Fecha fechaNacimiento;
		System.out.println("Introduce los datos del alumno:");
		do {
			nia = Teclado.leerInt("NIA (Nº Matricula): ");
			if(nia < 0) {
				System.out.println("Error. No puede ser un número negativo!");
			}
		} while (nia < 0);
		
		String nombre = Teclado.leerString("Nombre: ");
		String apellidos = Teclado.leerString("Apellidos: ");
		String dni = Teclado.leerString("DNI: ");
		do {
			System.out.println("Fecha de Nacimiento:");
			day = Teclado.leerInt("Día");
			month = Teclado.leerInt("Mes: ");
			year = Teclado.leerInt("Año: ");
			fechaNacimiento = new Fecha(day, month, year);
			if (fechaNacimiento.validarFecha(fechaNacimiento)) {
				System.out.println("Error. Fecha incorrecta!");
				fechaNacimiento = null;
			}
		} while (fechaNacimiento.validarFecha(fechaNacimiento));
		do {
			asignaturasTotales = Teclado.leerInt("Cantidad total de asignaturas que tendrá el alumno " + nombre + ": ");
			if(asignaturasTotales < 0) {
				System.out.println("Error. No puede ser un número negativo!");
			}
		} while (asignaturasTotales < 0);
		
		alumnos[posicionLibre] = new Alumno(nia, nombre, apellidos, dni, fechaNacimiento, asignaturasTotales);
	}

}
