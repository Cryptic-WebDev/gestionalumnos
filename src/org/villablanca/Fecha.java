package org.villablanca;
/**
 * Clase que representa una clase de tipo fecha
 * @author diegoc
 *
 */
public class Fecha {
/**
 * Creo @param entero para día
 * Creo @param entero para mes
 * Creo @param entero para año
 */
	private int day;
	private int month;
	private int year;
	
	public Fecha(int day, int month, int year) {
		
	this.day = day;
	this.month = month;
	this.year = year;
	
	}
	
	/**
	 * Método que valida la fecha introducida por parametro
	 * @param day Evalua si el dia es correcto
	 * @param month Evalua si la fecha es correcta
	 * @param year Evalua si el año es correcto
	 * @return Devuelve True si la fecha es correcta y False si no lo es
	 */
	public boolean validarFecha(Fecha fecha) {
		return day > 0 && day <= 31 && month > 0 && month <= 12 && year > 0;
	}
	
	/**
	 * Método Getter del atributo day
	 * @return Devuelve el valor del día de la fecha (int)
	 */
	public int getDay() {
		return day;
	}
	
	/**
	 * Método Getter del atributo month
	 * @return Devuelve el valor del mes de la fecha (int)
	 */
	public int getMonth() {
		return month;
	}
	
	/**
	 * Método Getter del atributo year
	 * @return Devuelve el valor del año de la fecha (int)
	 */
	public int getYear() {
		return year;
	}
}
